

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.setConnectedBody = false;
        this.enemy = null
    },

    start () {

    },

    onBeginContact(contact, selfCollider, otherCollider){
        if (this.enemy == null && otherCollider.node.name == "enemy"){
            this.enemy = otherCollider.node;
        }
    },

    onPresolve(contact, selfCollider, otherCollider){
        // contact.setTangentSpeed(-999);
    },

    update (dt) {
        if (this.enemy != null){
            this.enemy.position = cc.v2 (this.node.x, this.node.y + 15);
        }
    },
});
