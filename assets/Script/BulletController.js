var Global = require("Global");
cc.Class({
    extends: cc.Component,

    properties: {
        motion: {
            default: null,
            type: cc.Node,
        },
        isTheLast: false,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.rigid = this.node.getComponent(cc.RigidBody);
        this.circle = this.node.getComponent(cc.CircleCollider);
        this.redirect = false;
        this.preSpeed = null;
        var self = this;
        this.isExploding = false;
        this.explosion = this.node.getChildByName("explosive");
        this.node.isTheLast = false;


        this.node.on('gamepaused', (event) => {
            self.preSpeed = self.rigid.linearVelocity;
            self.rigid.linearVelocity = cc.v2();
        });

        this.node.on('gameplaying', (event) => {
            self.rigid.linearVelocity = self.preSpeed;
        });
        this.node.getWorldCoordinate = function () {
            return this.parent.convertToWorldSpace(this.position);
        };
        this.node.getParentCoordinate = function (position) {
            return this.parent.convertToNodeSpace(position);
        };
    },

    start() {
        this.setMotion();
    },

    setMotion() {
        this.motion = cc.instantiate(this.node.parent.getComponent("GamePlayController").motionPrefab);
        this.node.parent.addChild(this.motion);
    },

    onBeginContact(contact, selfCollider, otherCollider) {
        if (Global.soundOn && this.disappear != 0) cc.audioEngine.play(Global.busted, false, 0.1);
        this.redirect = true;
        if (this.disappear == 0 || Global.explosiveObjects.indexOf(otherCollider.node.name) != -1) {
            this.vanish();
        }
        if (otherCollider.node.name == "item") {
            // cc.log ("ddddddddddddddddddddddddddddd");
            otherCollider.node.removeFromParent(false);
        }
        if (!this.node.type) {
            this.disappear--;
        }

    },

    vanish() {
        cc.log(Global.bulletKilled);
        Global.bulletKilled = true;
        if (this.node.type == true && this.node.isTheLast == true && Global.aliveZombiesCount != 0) {
            this.node.parent.getComponent("GamePlayController").playRestartAction();
        }
        if (!this.node.type) {
            if (this.node.y > 700 || this.node.x > 900 || this.node.x < 0) {
                this.node.removeFromParent();
            } else {
                this.rigid.linearVelocity = cc.v2();
                this.isExploding = true;
                if (Global.soundOn) cc.audioEngine.play(Global.explode, false, 1);
                this.node.getComponent(cc.Animation).play("blast");
            }
        } else {
            this.node.removeFromParent();
        };
    },

    onEndContact(contact, selfCollider, otherCollider) {

    },

    onPreSolve(contact, selfCollider, otherCollider) {
    },

    onPostSolve(contact, selfCollider, otherCollider) {
    },

    onCollisionEnter(other, self) {
    },

    fire() {
        var self = this;
        if (this.node.type) {
            this.node.runAction(cc.sequence(cc.delayTime(Global.BULLET_LIFETIME), cc.callFunc(function () {
                self.disappear = 0;
            })));
        } else {
            this.disappear = 1;
        }
    },

    update(dt) {

        if (this.node.y > 700 || this.node.x > 900 || this.node.x < 0) {
            cc.log(this.node.x, this.node.y);
            this.vanish();
        };
        if (this.isExploding) {
            if (this.explosion.scale <= Global.BULLET_EXPLOSION_SCALE) {
                this.explosion.scale += 10;
            } else {
                this.rigid.active = false;
                this.explosion.active = false;
                if (this.node.isTheLast == true && Global.aliveZombiesCount != 0) {
                    this.node.parent.getComponent("GamePlayController").playRestartAction();
                }
            }
        }
        if (this.redirect == true) {
            this.redirect = false;
            this.node.rotation = - Math.atan2(this.rigid.linearVelocity.y, this.rigid.linearVelocity.x) * 180 / Math.PI;
        }
        if (this.motion != null) {
            this.motion.position = this.node.position;
        }
    },
});
