var Global = require("Global");
cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        cc.director.getCollisionManager().enabled = true;

    },

    start() {

    },
    onCollisionEnter(other, self) {
        var world = self.world;
        var bullet = other.node.parent;
        var bulletBody = bullet.getComponent(cc.RigidBody);
        var linearVelocity = bulletBody.linearVelocity;
        bulletBody.linearVelocity = cc.v2();
        var currentPosition = this.node.convertToNodeSpace(bullet.getWorldCoordinate());
        bullet.getComponent("BulletController").motion.removeFromParent(false);
        bullet.active = false;

        this.node.outPort.getComponent("TeleportController").bulletOut(bullet, currentPosition, linearVelocity);
    },

    bulletOut(bullet, position, linearVelocity) {
        // cc.log("bullet Out");
        var newPosition = this.node.convertToWorldSpace(position);
        newPosition = bullet.getParentCoordinate(newPosition);
        var bulletBody = bullet.getComponent(cc.RigidBody);

        if (this.node.rotation == 0) {
            newPosition = cc.v2(newPosition.x, newPosition.y - 10);
        } else if (this.node.rotation == -180) {
            newPosition = cc.v2(newPosition.x, newPosition.y + 30);
        } else if (this.node.rotation == -90) {
            newPosition = cc.v2(newPosition.x - 10, newPosition.y);
            linearVelocity = cc.v2(-linearVelocity.x, -linearVelocity.y);
        }

        bullet.position = newPosition;
        bullet.active = true;
        bulletBody.linearVelocity = linearVelocity;
        bullet.getComponent("BulletController").setMotion();
    }
    // update (dt) {},
});
