var Global = require('Global');

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        Global.resourceLoader = this;

        Global.sf_bg_city = this.loadSpriteFrame("bgpic/bg_0.png");
        Global.sf_bg_jungle = this.loadSpriteFrame("bgpic/bg_1.png");
        Global.sf_bg_lab = this.loadSpriteFrame("bgpic/bg_2.png");
        Global.sf_bg_desert = this.loadSpriteFrame("bgpic/bg_3.png");

    },

    loadSpriteFrame(path) {
        return new cc.SpriteFrame(cc.textureCache.addImage(cc.url.raw("resources/" + path)));
    },

    loadPrefabs() {
        cc.loader.loadResDir("Prefabs/", cc.Prefab, function (err, prefabs) {
            if (err != null) {
                cc.log(err);
                return;
            }
            Global.prefabs = prefabs;
            cc.log(Global.prefabs[1].data.name);
        })
    },

    loadMaps() {
        cc.loader.loadRes("json/" + "m" + Global.selectedMode + "l" + Global.selectedLevel, function (err, data) {
            if (err) {
                cc.log(err);
                return;
            } else {
                Global.selectedMap = data.elements;
            }
        });
    },

    loadSkins(a, b) {
        Global.skinStanding = [];
        Global.skinMoving = [];
        Global.spineStanding = a;
        Global.spineMoving = b;
        cc.loader.loadResDir("zombies/zombie" + a, cc.SpriteFrame, function (err, assets) {
            if (err) {
                cc.log(err);
                return;
            } else {
                Global.skinStanding = assets;
            }
        });
        cc.loader.loadResDir("zombies/zombie" + b, cc.SpriteFrame, function (err, assets) {
            if (err) {
                cc.log(err);
                return;
            } else {
                Global.skinMoving = assets;
            }
        });
    }

    // start() {},

    // update (dt) {},
});
