var Global = require("Global");
cc.Class({
    extends: cc.Component,

    properties: {
        spine: cc.Node,
        skin: cc.Node,
        collider: cc.Node,
        type: true,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.body = this.node.getComponent(cc.RigidBody);
        this.isMovingLeft = true;
        this.enemyColliderController = this.collider.getComponent("EnemyColliderController");
        this.ground = [];
        this.setConnectedBody = false;
        // this.body.linearVelocity = cc.v2(-200, 0);
    },

    start() {

        this.skin.children.forEach(element => {
            // element.getComponent(cc.PhysicsPolygonCollider).enabled = false;    
        });

        // this.playAliveAnim();

    },

    playAliveAnim() {
        if (this.type) {
            // this.body.type = cc.RigidBodyType.Static;
            this.spine.getComponent(sp.Skeleton).animation = "standing";
            this.spine.getComponent(sp.Skeleton).setSkin("zombie" + Global.spineStanding);

        } else {
            this.spine.getComponent(sp.Skeleton).setSkin("zombie" + Global.spineMoving);
            this.spine.getComponent(sp.Skeleton).animation = "limping";
            this.node.getComponent(cc.PhysicsBoxCollider).friction = 0;
        }
    },


    setSkin() {

        for (var i = 0; i < this.skin.children.length; i++) {
            var spriteFrame = this.findSpriteFrame(this.skin.children[i].name);
            if (spriteFrame != undefined) {
                this.skin.children[i].getComponent(cc.Sprite).spriteFrame = spriteFrame;
            }
        }
    },

    findSpriteFrame(name) {
        name = String(name);
        if (this.type) {
            return Global.skinStanding.find(function (element) {
                return element._name == name;
            });

        } else {
            return Global.skinMoving.find(function (element) {
                return element._name == name;
            });
        }
    },

    onBeginContact(contact, selfCollider, otherCollider) {
        if (this.ground.length == 0 && this.type == false &&
            (Math.abs(otherCollider.node.rotation) == 0)
            || (Math.abs(otherCollider.node.rotation) == 90 )) {
            this.ground.push(otherCollider.node.uuid);
        }

    },

    // onEndContact(contact, selfCollider, otherCollider){
    // },
    
    onPreSolve(contact, selfCollider, otherCollider) {
        if (this.type == false) {
            var box = otherCollider.node.getBoundingBox();

            if (otherCollider.node.uuid == this.ground[0] && box.width > 49) {
                if (this.node.x <= box.xMin + 10) {
                    this.body.linearVelocity = cc.v2(Global.ZOMBIE_VELOCITY, 0);
                    this.node.runAction(cc.flipX(true));
                    this.isMovingLeft = false;

                }
                else if (this.node.x >= box.xMax - 10) {
                    this.body.linearVelocity = cc.v2(-Global.ZOMBIE_VELOCITY, 0);
                    this.isMovingLeft = true;
                    this.node.runAction(cc.flipX(false));
                    // cc.log("trường hợp 1.2");
                } else {
                    // cc.log("trường hợp 1.3");
                    if (this.isMovingLeft) {
                        this.body.linearVelocity = cc.v2(-Global.ZOMBIE_VELOCITY, 0);
                    } else {
                        this.body.linearVelocity = cc.v2(Global.ZOMBIE_VELOCITY, 0);
                    }
                }
            } else if(otherCollider.node.uuid == this.ground[0] && box.width <49){
                this.spine.getComponent(sp.Skeleton).animation = "standing";
            }
            if (otherCollider.node.uuid != this.ground[0]) {
                    if (this.isMovingLeft) {
                        this.isMovingLeft = false;
                        this.node.runAction(cc.flipX(true));
                        this.body.linearVelocity = cc.v2(Global.ZOMBIE_VELOCITY, 0);
                    } else {
                        this.body.linearVelocity = cc.v2(-Global.ZOMBIE_VELOCITY, 0);
                        this.node.runAction(cc.flipX(false));
                        this.isMovingLeft = true;
                    }
                // }
            }

        }

    },

    // will be called every time collider contact is resolved
    onPostSolve(contact, selfCollider, otherCollider) {

    },

    playDead() {
        if (Global.soundOn) cc.audioEngine.play(Global.killed, false, 1);
        this.skin.children.forEach(component => {
            if(component.getChildByName("blood")!= null){
                component.getChildByName("blood").getComponent(cc.Animation).play();
            };
            component.getComponent(cc.RigidBody).linearVelocity = this.getLinearVelocity();
        })
    },

    getLinearVelocity() {
        var x = this.randomNumber(-50, 50);
        var y = this.randomNumber(-50, 50);
        return cc.v2(x, y);
    },

    randomNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    },

    update(dt) {
    },
});
