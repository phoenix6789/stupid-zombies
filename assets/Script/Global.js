
const BULLET_SPEED = 600;
const LAZER0 = cc.color(255, 22, 22);
const LAZER1 = cc.color(70, 255, 70);
const LAZER2 = cc.color(240, 255, 70);
const ANGLE = 20;
const HEADSHOT = 2000;
const BODYSHOT = 1000;
const REMAIN_BULLET = 3000;
const ACTIVATED_MODE = "ACTIVATED_MODE";
const ACTIVATED_LEVEL = "ACTIVATED_LEVEL";
const ZOMBIE_VELOCITY = 27;
const EXPLOSIVE_RADIUS = 6;
const BULLET_EXPLOSION_SCALE = 60;
const EXPLOSIVE_SPEED = 500;
const ZOMBIE_WIDTH = 25;
const ENEMY_COLLIDER = "collider";
const BULLET_LIFETIME = 3.4;
const GAME_STATE = cc.Enum({
    PAUSE: 0,
    PLAYING: 1,
    COMPLETED: 2,
    WINNED: 3
});
module.exports = {

    BULLET_SPEED: BULLET_SPEED,
    LAZER0: LAZER0,
    LAZER1: LAZER1,
    LAZER2: LAZER2,
    GAME_STATE: GAME_STATE,
    ANGLE: ANGLE,
    HEADSHOT: HEADSHOT,
    BODYSHOT: BODYSHOT,
    REMAIN_BULLET: REMAIN_BULLET,
    ACTIVATED_MODE: ACTIVATED_MODE,
    ACTIVATED_LEVEL: ACTIVATED_LEVEL,
    ZOMBIE_VELOCITY: ZOMBIE_VELOCITY,
    EXPLOSIVE_RADIUS: EXPLOSIVE_RADIUS,
    EXPLOSIVE_SPEED: EXPLOSIVE_SPEED,
    ZOMBIE_WIDTH: ZOMBIE_WIDTH,
    ENEMY_COLLIDER: ENEMY_COLLIDER,
    BULLET_EXPLOSION_SCALE: BULLET_EXPLOSION_SCALE,
    BULLET_LIFETIME: BULLET_LIFETIME,
    //------------------------------------------------------
    Mode: null,
    activatedMode: null, // activation status of modes init by [1,0,0,0]
    activatedLevels: null,
    listMaps: [],
    sf_bg: [],
    skinStanding: [],
    skinMoving: [],
    indexOfSelectedLevel: 0,
    selectedLevel: -1,
    selectedMode: -1,
    selectedMap: null,
    menuSceneLoadMode: true,
    gameState: GAME_STATE.PLAYING,
    aliveZombiesCount: 0,
    score: 0,
    maxScore: 0,
    spineStanding: -1,
    spineMoving: -1,
    horizontalPlatform: ["platform0", "platform10", "platform12", "platform3", "platform4", "platform5", "platform9", "left0", "left1", "left2", "left3", "right0", "right1", "right2", "right3", "drum0", "drum1"],
    explosiveObjects: ["bomb0", "bomb1", "drum0", "drum1"],
    killerObjects: ["bullet", "ironbeam", "wood", "stone0", "stone1", "tyre0", "tyre1", "tyre2", "explosive","jointtyre1"],
    bombs: ["bomb0", "bomb1", "drum0", "drum1"],
    bulletKilled: false,
    isTest: true,

    //------------------------------------------------------  

    appear: cc.fadeIn(0.1),
    disappear: cc.fadeOut(0.1),
    easing: cc.repeatForever(cc.sequence( cc.scaleTo(0.5, 0.8), cc.scaleTo(0.5,0.6))),

    //------------------------------------------------------

    resourceLoader: null,
    menuScene: null,
    gameScene: null,
    gamePlayController: null,

    //------------------------------------------------------

    sf_bg_city: null,
    sf_bg_jungle: null,
    sf_bg_lab: null,
    sf_bg_desert: null,

    //------------------------------------------------------
    busted: null,
    click: null,
    complete: null,
    explode: null,
    killed: null,
    music: null,
    star: null,
    gun: null,
    gunLoad: null,
    soundOn: true,
    volume: 1,
}