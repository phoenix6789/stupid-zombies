var Global = require('Global');
const SHELL_TYPE = cc.Enum({
    NORMAL: 0,
    TRIPLE: 1,
    EXPLOSIVE: 2,
})
cc.Class({
    extends: cc.Component,

    properties: {
        loadingNode: cc.Node,
        hero: cc.Node,
        upperBody: cc.Node,
        bullets: {
            type: cc.Node,
            default: []
        },

        touchHandler: cc.Node,
        helpScreen: cc.Node,
        bulletPrefab: cc.Prefab,
        motionPrefab: cc.Prefab,
        p1: cc.Node,
        p2: cc.Node,
        spOutOfAmmo: cc.Node,
        btnRetry: cc.Node,
        btnWatchAds: cc.Node,
        levelTile: cc.Label,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.initiateGame();
    },

    initiateGame() {
        Global.maxScore = 0;
        Global.bulletKilled = true;
        this.prefabs = [];
        this.urls = [];
        this.level = Global.selectedLevel;
        this.gunshells = [];
        this.map = Global.selectedMap;
        this.finishLoading = false;
        this.loadingNode.active = true;
        this.loadingNode.opacity = 255;
        this.hero.position = cc.v2(0, 0);
        this.graphics = this.node.getComponent(cc.Graphics);
        this.heroController = this.hero.getComponent("HeroController");
        this.shotCount = 0;
        this.fired = false;
        this.movingNode = null;
        this.bulletsCount = 0;
        this.enemyCount = 0;
        this.shellType = SHELL_TYPE.NORMAL;
        this.isOutOfAmmo = false;
        this.spOutOfAmmo.active = false;
        this.isPlayingOutOfAmmoStatus = false;
        this.btnRetry.children[0].active = false;
        this.adsPlayed = false;
        this.btnWatchAds.active = false;
        this.additionalBullet = null;
        var level = Global.selectedLevel + 1;
        this.levelTile.string = "Level " + level + " - " + this.getModeTitle();
        this.levelTile.node.zIndex = 1000;
        Global.aliveZombiesCount = 0;
        Global.score = 0;
        Global.gamePlayController = this;

    },

    start() {
        var self = this;
        this.loadElements();
        this.setEventToucheHandler();
    },

    getModeTitle() {
        switch (Global.selectedMode) {
            case Global.Mode.CITY: {
                return "CITY";
            }
            case Global.Mode.JUNGLE: {
                return "JUNGLE";
            }
            case Global.Mode.LAB: {
                return "LAB";
            }
            case Global.Mode.DESERT: {
                return "DESERT";
            }
        }
    },

    setEventToucheHandler() {
        var self = this;
        this.btnWatchAds.on(cc.Node.EventType.TOUCH_START, event => {

        }, this.btnWatchAds);
        if (this.touchHandler) {
            this.touchHandler.on(cc.Node.EventType.TOUCH_START, event => {
                if (Global.gameState != Global.GAME_STATE.PAUSE && self.helpScreen.active == false
                    && Global.bulletKilled == true) {
                    if (Global.soundOn) cc.audioEngine.play(Global.gunLoad, false, 1);
                    var touches = event.getTouches();
                    var touchLoc = touches[0].getLocation();
                    self.heroController.aim(touchLoc);
                    self.reloadBullet();
                    if (self.shotCount == 0) {
                        self.onOutOfAmmo();
                    }
                }
            }, this);
            this.touchHandler.on(cc.Node.EventType.TOUCH_END, event => {

                if (Global.gameState != Global.GAME_STATE.PAUSE && self.shotCount > 0
                    && self.helpScreen.active == false && Global.bulletKilled == true) {
                    if (Global.soundOn) cc.audioEngine.play(Global.gun, false, 1);
                    self.heroController.fire();
                    self.heroController.cross.opacity = 0;
                    self.fired = true;
                    self.fire(this.shellType);
                }
                self.heroController.upperBodyGraphics.clear();

            }, this);
            this.touchHandler.on(cc.Node.EventType.TOUCH_MOVE, event => {
                if (Global.gameState != Global.GAME_STATE.PAUSE && self.helpScreen.active == false) {
                    var touches = event.getTouches();
                    var touchLoc = touches[0].getLocation();
                    self.heroController.aim(touchLoc);
                }
            }, this);
        }
    },

    loadElements() {
        var self = this;
        for (var i = 0; i < this.map.length; i++) {
            var url = "Prefabs/" + this.map[i].elementType;
            this.urls.push(url);
        }
        cc.loader.loadResArray(self.urls, cc.Prefab, function (err, resources) {
            if (err) {
                cc.log(err);
                return;
            } else {
                self.prefabs = resources;
                self.urls = [];
            }
        });

    },

    setupMap() {
        var jointtyre;
        // this.heroController.upperBody.rotation = 0;
        // this.heroController.maincontroller.rotation = 0;
        for (var i = 0; i < this.map.length; i++) {
            if (this.map[i].elementType != "hero" && this.map[i].elementType != "movinginfor") {
                // cc.log (this.prefabs[i]);
                var node = cc.instantiate(this.prefabs[i]);
                this.node.addChild(node);
                node.rotation = -this.map[i].rotation;
                if (node.name != "enemy" && node.name.indexOf("guns") == -1) {
                    node.scaleX = this.map[i].width / node.width;
                    node.scaleY = this.map[i].height / node.height;
                }
                node.position = cc.v2(this.map[i].x, this.map[i].y);
                if (node.name == "movingplatform") {
                    this.movingNode = node;
                }
                if (node.name == "enemy") {
                    this.enemyCount++;
                    Global.aliveZombiesCount++;
                    node.getComponent("EnemyController").type = this.map[i].height > 30;
                    node.getComponent("EnemyController").playAliveAnim();
                    node.getComponent("EnemyController").setSkin();
                }
                if (node.name == "jointtyre1") {
                    cc.log(node.getComponents(cc.RevoluteJoint));
                }

            }

        }
        this.setHero();
        this.setGunshell();
        this.setBullets();
        this.setTeleports();
        this.setPipes();
        this.setJointTyre();
        this.setMovingPlatform();
        Global.maxScore = Global.maxScore + (this.gunshells.length - 1) * Global.REMAIN_BULLET
            + Global.aliveZombiesCount * Global.HEADSHOT;
    },

    setHero() {
        var _hero = this.map.filter(function (element) {
            return element.elementType == "hero";
        })[0];
        this.hero.zIndex = 1000;
        this.hero.position = cc.v2(_hero.x, _hero.y - 36);
        this.hero.rotation = -_hero.rotation;
    },

    setGunshell() {
        this.gunshells = this.node.children.filter((node) => {
            return node.name.indexOf("gunshel") != -1;
        });
        var bottom = this.node.children.filter((node) => {
            return node.name.indexOf("bot") != -1;
        })[0];

        for (var i = 0; i < this.gunshells.length; i++) {
            this.gunshells[i].zIndex = 5000;
            this.gunshells[i].x = 60 + 30 * i;
            this.gunshells[i].y = bottom.y + 40;
            if (this.gunshells[i].name == "gunshell" || this.gunshells[i].name == "gunshell2") {
                this.bulletsCount += 1;
            }
            if (this.gunshells[i].name == "gunshell1") {
                this.bulletsCount += 4;
            }
            this.bulletsCount++;
        }
        this.setAdditionalBullet();
        this.shotCount = this.gunshells.length;
    },

    setAdditionalBullet() {

        var explShells = this.prefabs.filter((node) => {
            return node._name == "gunshell2";
        });
        var normShells = this.prefabs.filter((node) => {
            return node._name == "gunshell";
        });
        var tripShells = this.prefabs.filter((node) => {
            return node._name == "gunshell1";
        });
        if (explShells.length != 0) {
            this.additionalBullet = cc.instantiate(explShells.pop());
        } else if (tripShells.length != 0) {
            this.additionalBullet = cc.instantiate(tripShells.pop());
        } else {
            this.additionalBullet = cc.instantiate(normShells.pop());
        }

        //-----------------------------------
        this.node.addChild(this.additionalBullet);
        this.additionalBullet.position = this.gunshells[0].position;
        this.additionalBullet.scale = this.gunshells[0].scale = 1.5;
        this.additionalBullet.active = false;
    },

    reloadBullet() {
        if (this.gunshells[0] != null) {
            var gunshell = this.gunshells.shift();
            if (gunshell.name == "gunshell") {
                this.shellType = SHELL_TYPE.NORMAL;
            }
            if (gunshell.name == "gunshell1") {
                this.shellType = SHELL_TYPE.TRIPLE;
            }
            if (gunshell.name == "gunshell2") {
                this.shellType = SHELL_TYPE.EXPLOSIVE;
            }
            gunshell.removeFromParent(false);
            if (this.gunshells[0] != null) {
                this.gunshells[0].runAction(cc.scaleTo(0.2, 1.5));
            }
        }
    },

    setBullets() {
        for (var i = 0; i < this.bulletsCount; i++) {
            var bullet = cc.instantiate(this.bulletPrefab);
            this.node.addChild(bullet);
            this.bullets.push(bullet);
            bullet.position = this.heroController.getP2WorldSpace();
            bullet.active = false;
        }
    },

    setTeleports() {
        var ports = this.node.children.filter((node) => {
            return node.name.indexOf("reflect") != -1;
        });
        // set outport
        if (ports.length > 0) {
            ports[0].outPort = ports[1];
            ports[1].outPort = ports[0];
        }

    },

    setPipes() {
        var piperuns = this.node.children.filter((node) => {
            return node.name == "piperun";
        });
        var endpipes = this.node.children.filter((node) => {
            return node.name == "endjoint";
        });
        if (piperuns.length > 0 && endpipes.length > 0) {
            piperuns[0].endjoint = endpipes[1];
            piperuns[1].endjoint = endpipes[0];
        }
    },

    setJointTyre() {
        var ropes = this.node.children.filter((node) => {
            return node.name == "rope";
        });
        var jointtyres = this.node.children.filter((node) => {
            return node.name == "jointtyre1";
        });
        if (ropes.length == 1) {
            jointtyres[0].getComponents(cc.RevoluteJoint)[0].connectedBody =
                ropes[0].children[ropes[0].children.length - 1].getComponent(cc.RigidBody);
            jointtyres[0].getComponents(cc.RevoluteJoint)[0].connectedAnchor = cc.v2(0, -17);
        }
        if (ropes.length == 3) {
            jointtyres[0].getComponents(cc.RevoluteJoint)[0].connectedBody =
                ropes[0].children[ropes[0].children.length - 1].getComponent(cc.RigidBody);
            jointtyres[0].getComponents(cc.RevoluteJoint)[0].connectedAnchor = cc.v2(0, -17);

            jointtyres[0].getComponents(cc.RevoluteJoint)[1].connectedBody =
                ropes[2].children[ropes[2].children.length - 1].getComponent(cc.RigidBody);
            jointtyres[0].getComponents(cc.RevoluteJoint)[1].connectedAnchor = cc.v2(0, -17);

            jointtyres[1].getComponents(cc.RevoluteJoint)[0].connectedBody =
                ropes[1].children[ropes[1].children.length - 1].getComponent(cc.RigidBody);
            jointtyres[1].getComponents(cc.RevoluteJoint)[0].connectedAnchor = cc.v2(0, -17);
        }

    },

    setMovingPlatform() {
        var movingplatform = this.node.children.filter((node) => {
            return node.name == "movingplatform";
        });
        var movingplatform1 = this.node.children.filter((node) => {
            return node.name == "movingplatform1";
        });

        if (Global.selectedMode == 2) {
            switch (Global.selectedLevel) {
                case 1: {

                    var action = cc.repeatForever(cc.sequence(cc.moveBy(5, cc.v2(-280, 0)), cc.moveBy(5, cc.v2(280, 0))));
                    movingplatform[0].runAction(action);
                    break;
                }
                case 7: {
                    var actions1 = [];
                    var actions2 = [];
                    for (var i = 0; i < 3; i++){
                        var action = cc.repeatForever(cc.sequence(cc.moveBy(4, cc.v2(-200, 0)), cc.moveBy(4, cc.v2(200, 0))));
                        
                        var action2 = cc.repeatForever(cc.sequence(cc.moveBy(4, cc.v2(200, 0)), cc.moveBy(4, cc.v2(-200, 0))));
                        
                        actions1.push(action);
                        actions2.push(action2);
                    }
                    
                    for (var i = 0; i < movingplatform.length; i++) {
                        movingplatform[i].rotation = 0;
                        if (movingplatform[i].y > 400) {
                            movingplatform[i].runAction(actions1.pop());
                        } else {
                            movingplatform[i].runAction(actions2.pop());
                        }
                    }
                    break;
                }
                case 8: {
                    var actionMP10 = cc.repeatForever(cc.sequence(cc.moveBy(3, cc.v2(-140, 0)), cc.moveBy(3, cc.v2(140, 0))));
                    var actionMP11 = cc.repeatForever(cc.sequence(cc.moveBy(3, cc.v2(140, 0)), cc.moveBy(3, cc.v2(-140, 0))));
                    movingplatform1[0].runAction(actionMP10);
                    movingplatform1[1].runAction(actionMP11);
                    break;
                }
                case 9: {

                    var actionMP10 = cc.repeatForever(cc.sequence(cc.moveBy(3, cc.v2(0, -240)), cc.moveBy(3, cc.v2(0, 240))));
                    var actionMP11 = cc.repeatForever(cc.sequence(cc.moveBy(3, cc.v2(0, 240)), cc.moveBy(3, cc.v2(0, -240))));
                    movingplatform1[1].runAction(actionMP10);
                    movingplatform1[0].runAction(actionMP11);

                    var actionMP1 = cc.repeatForever(cc.sequence(cc.moveBy(3, cc.v2(0, 140)), cc.moveBy(3, cc.v2(0, -140))));
                    movingplatform[0].runAction(actionMP1);
                    movingplatform[1].runAction(actionMP1.clone());
                    break;
                }
            }
        }
    },

    setConnectedBody() {
        var ropes = this.node.children.filter((node) => {
            return node.name == "rope";
        });
        var jointtyres = this.node.children.filter((node) => {
            return node.name == "jointtyre1";
        });

        jointtyres[0].getComponents(cc.RevoluteJoint)[0].connectedBody =
            ropes[0].children[ropes[0].children.length - 1].getComponent(cc.RigidBody);
        jointtyres[0].getComponents(cc.RevoluteJoint)[0].connectedAnchor = cc.v2(0, -17);

        jointtyres[0].getComponents(cc.RevoluteJoint)[1].connectedBody =
            ropes[2].children[ropes[2].children.length - 1].getComponent(cc.RigidBody);
        jointtyres[0].getComponents(cc.RevoluteJoint)[1].connectedAnchor = cc.v2(0, -17);
    },

    fire(type) {

        if (this.fired == true && this.shotCount != 0 && type == SHELL_TYPE.TRIPLE) {
            this.shotCount--;
            this._fire(0, type);
            this._fire(1, type);
            this._fire(2, type);
            this.fired = false;
            // }if (this.fired == true && this.shotCount != 0 && type == SHELL_TYPE.NORMAL) {
        } else {
            this.shotCount--;
            this._fire(0, type);
            this.fired = false;
        }

    },

    _fire(i, type) {
        var bullet = this.bullets.pop();
        var rad = this.heroController.getBulletRotation();
        var rvec = 0; // linearVelocity's angle in radian;

        bullet.position = this.node.convertToNodeSpace(this.heroController.getP2WorldSpace());
        if (i == 0) {
            rvec = rad;
            bullet.rotation = -rad * 180 / Math.PI;
            Global.bulletKilled = false;
            if (this.shotCount == 0 && type != SHELL_TYPE.TRIPLE) {
                // when this flag is true, the btnWatchAds button will be shown after the bullet disappeared
                bullet.isTheLast = true;
            };
        }
        if (i == 1) {
            rvec = rad + Global.ANGLE * Math.PI / 180;
            bullet.rotation = -rad * 180 / Math.PI + Global.ANGLE;
        }
        if (i == 2) {
            rvec = rad - Global.ANGLE * Math.PI / 180;
            bullet.rotation = -rad * 180 / Math.PI - Global.ANGLE;
            if (this.shotCount == 0 && type == SHELL_TYPE.TRIPLE) {
                bullet.isTheLast = true;
            };
        }
        if (type == SHELL_TYPE.EXPLOSIVE) {
            bullet.type = false;
        } else bullet.type = true;
        bullet.active = true;
        bullet.getComponent("BulletController").fire();
        var vec = cc.v2(Math.cos(rvec), Math.sin(rvec)); // calculate the linearVelocity
        bullet.getComponent(cc.RigidBody).linearVelocity = vec.normalize().mulSelf(Global.BULLET_SPEED);
    },

    linearEquation(x, _p1, _p2) {
        return cc.v2(x, ((_p2.y - _p1.y) / (_p2.x - _p1.x)) *
            (x - _p1.x) + _p1.y);
    },

    onWinned() {
        var self = this;
        this.onWinnedAction = cc.sequence(cc.delayTime(0.5),
            cc.callFunc(function () {
                self.gunshells.forEach(shell => {
                    shell.getComponent(cc.Animation).play();
                    Global.score += Global.REMAIN_BULLET;
                });
            }), cc.delayTime(3),
            cc.callFunc(function () {
                Global.gameScene.onGameCompleted();
            }))
        this.node.runAction(this.onWinnedAction);
    },


    onReplayTouched() {
        if (this.helpScreen.active == false) {
            this.node.stopAction(this.onWinnedAction);
            this.btnRetry.stopAction(this.retryAction);
            Global.gameState = Global.GAME_STATE.PLAYING;
            var self = this;
            Global.score = 0;
            Global.gameScene.lbScore.string = 0;
            this.node.removeAllChildren();
            this.node.removeChild(this.node.children[i]);
            this.node.addChild(this.hero);
            this.heroController.resetRotation();
            this.initiateGame();
            this.loadElements();
        }
    },

    onNextLevelTouched() {
        this.btnRetry.stopAction(this.retryAction);
        this.heroController.resetRotation();
    },

    playRestartAction() {
        this.retryAction = cc.repeatForever(cc.sequence(cc.scaleTo(0.5, 0.8), cc.scaleTo(0.5, 1)));
        // retryAction.setTag(1);
        var watchAdsAction = cc.repeatForever(cc.sequence(cc.scaleTo(0.8, 0.6), cc.scaleTo(0.8, 0.8)));
        // watchAdsAction.setTag(2);
        this.btnRetry.children[0].active = true;
        cc.director.getActionManager().removeAllActionsFromTarget(this.btnRetry, true);
        this.btnRetry.runAction(this.retryAction);
        if (this.adsPlayed == false) {
            this.adsPlayed = true;
            this.btnWatchAds.active = true;
            this.btnWatchAds.runAction(watchAdsAction);
        }

    },

    onOutOfAmmo() {
        var self = this;
        if (this.shotCount == 0) {
            this.spOutOfAmmo.active = true;
            this.spOutOfAmmo.runAction(cc.sequence(cc.delayTime(0.9), cc.callFunc(function () {
                self.spOutOfAmmo.active = false;
            })));
        }
    },

    onWatchAdsTouched() {
        this.btnWatchAds.stopActionByTag(1);
        this.btnRetry.stopAction(this.retryAction);
        this.btnWatchAds.active = false;
        this.btnRetry.children[0].active = false;
        this.shotCount++;
        this.additionalBullet.active = true;
        this.additionalBullet.isTheLast = true;
        this.gunshells.push(this.additionalBullet);
    },



    update(dt) {

        if (this.prefabs.length > 1 && this.finishLoading == false) {
            this.finishLoading = true;
            var self = this;
            this.setupMap();
            this.loadingNode.runAction(cc.sequence(cc.fadeOut(0.5), cc.callFunc(function () {
                self.loadingNode.active = false;
            })));
        }
        // cc.log (this.movingNode.position);
    },
});
