var Global = require("Global");

cc.Class({
    extends: cc.Component,

    properties: {
        // blast:cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

        this.body = this.node.getComponent(cc.RigidBody);
        this.explosion = this.node.getChildByName("explosive");
        this.explosionController = this.explosion.getComponent("ExplosionController");
        this.physicsCollider = this.node.getComponent(cc.PhysicsPolygonCollider);
        this.node.blasted = false;
        this.blast = null;
        var self = this;
        cc.loader.loadRes("Prefabs/blast", cc.Prefab, function (err, prefab) {
            var blast = cc.instantiate(prefab);
            self.blast = blast;
            self.node.addChild(self.blast);
            self.blast.position = cc.v2();
            self.blast.opacity = 0;
        });
        this.explosion.getComponent(cc.RigidBody).active = false;
    },

    start() {

    },

    onBeginContact(contact, selfCollider, otherCollider) {

        if (otherCollider.node.name == "bullet") {
            if (Global.soundOn) cc.audioEngine.play (Global.explode, false, 1);
            this.node.blasted = true;
            this.explosionController.isExploding = true;
        }
    },

    blastAnimate() {
        this.node.getComponent(cc.Sprite).enabled = false;
        if (this.blast != null) {
            this.blast.opacity = 255;
            this.blast.getComponent(cc.Animation).play();
        }
    }

    // update(dt) {
    // },
});
