var Global = require("Global");

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.parentController = this.node.parent.getComponent("EnemyController");
        this.isDead = false
    },

    start() {

    },

    onCollisionEnter(other, self) {
        if (Global.killerObjects.indexOf(other.node.name) != -1 && this.isDead == false) {
            if (self.tag == 1 && other.node.parent.name == "bullet") {
                Global.score += Global.HEADSHOT;
                this.parentController.node.getComponent(cc.Animation).play('head_shot');
            } else {
                Global.score += Global.BODYSHOT;
                this.parentController.node.getComponent(cc.Animation).play('body_shot')
            }
            Global.gameScene.updateScore(Global.score);
            Global.aliveZombiesCount--;
            if (Global.aliveZombiesCount == 0) {
                this.isTheLast = true;
                Global.gameState = Global.GAME_STATE.WINNED;
                Global.gamePlayController.onWinned();
            }
            this.parentController.skin.active = true;
            this.parentController.spine.active = false;
            this.parentController.playDead();
            this.isDead = true
            this.node.getComponent(cc.PolygonCollider).enabled = false;
            this.node.getComponent(cc.CircleCollider).enabled = false;
        }
    },



    // update (dt) {},
});
