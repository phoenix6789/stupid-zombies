var Global = require('Global');

cc.Class({
    extends: cc.Component,

    properties: {
        background: cc.Sprite,
        gamePlay: cc.Node,
        gamePause: cc.Node,
        gameComplete: cc.Node,
        lbScore: cc.Label,
        lbHighScore: cc.Label,
        starsPause: cc.Node,
        starsComplete: cc.Node,
        whitestars: cc.Node,
        lbNextLevel: cc.Label,
        particle: cc.Prefab,
    },

    onLoad() {
        if (Global.soundOn) cc.audioEngine.stopAll();
        Global.gameScene = this;
        this.n_control = this.node.getChildByName("controll");
        this.helpScreen = this.node.getChildByName("help");
        this.helpScreen.active = false;
        this.helped = cc.sys.localStorage.getItem("helped");
        if (this.helped == null) {
            cc.sys.localStorage.setItem("helped", 1);
        }
        this.mode = Global.selectedMode;
        this.score = 0;
        this.highScore = 0;
        this.loadedMap = false;
        if (this.mode == null) {
            this.setBackground(Global.Mode.CITY);
        }
        else this.setBackground(this.mode);
        this.node.runAction(Global.appear);
        cc.director.getPhysicsManager().enabled = true;
        // cc.director.getPhysicsManager().debugDrawFlags = true;
        cc.director.getCollisionManager().enabled = true;
        // cc.director.getCollisionManager().enabledDebugDraw  = true;

        this.gameComplete.active = true;
        this.gameComplete.position = cc.v2();
        this.lbFinalScore = this.gameComplete.getChildByName("high score").getComponent(cc.Label);


    },

    start() {
        this.lbScore.string = 0;
        cc.log(Global.selectedMode, Global.selectedLevel)
        if (Global.selectedMode == 0 && Global.selectedLevel == 0 && this.helped == null) {
            cc.log("ddddddddddddddddddddddd");
            this.helpScreen.active = true;
        }
        this.initStars();
    },

    initStars() {
        for (var i = 0; i < 3; i++) {
            this.starsPause.children[i].active = false;
            this.starsComplete.children[i].scale = 0;
        }
    },

    runCompleteStarsAction(star) {
        var action = cc.scaleTo(0.4, 1).easing(cc.easeBackOut());
        for (var i = 0; i < star; i++) {
            var par = this.starsComplete.children[i].getChildByName("particle");
            if (par == null) {
                par = cc.instantiate(this.particle);
                this.starsComplete.children[i].addChild(par);
            }

            this.starsComplete.children[i].runAction(cc.sequence(cc.delayTime(i * 0.4), cc.callFunc(function () {
                if (Global.soundOn) cc.audioEngine.play(Global.star, false, 1);
            }), action));
        }

    },

    setBackground(mode) {
        switch (mode) {
            case Global.Mode.CITY: {
                this.background.spriteFrame = Global.sf_bg_city;
                break;
            }
            case Global.Mode.JUNGLE: {
                this.background.spriteFrame = Global.sf_bg_jungle;
                break;
            }
            case Global.Mode.LAB: {
                this.background.spriteFrame = Global.sf_bg_lab;
                break
            }
            case Global.Mode.DESERT: {
                this.background.spriteFrame = Global.sf_bg_desert;
                break;
            }
        }
    },

    onBackTouched() {
        cc.director.loadScene('Menu Scene');
        Global.menuSceneLoadMode = false;
        Global.gameState = Global.GAME_STATE.PLAYING;

    },

    onPauseTouched() {
        if (Global.soundOn) cc.audioEngine.play(Global.click, false, 1);
        if (this.helpScreen.active == false) {
            for (var i = 0; i < 3; i++) {
                this.starsPause.children[i].active = false;
            }
            this.highScore = Global.activatedLevels[Global.indexOfSelectedLevel].highScore;
            if (this.highScore != undefined) {
                this.highScore = 0;
            }
            this.lbHighScore.string = "HIGH SCORE: " + this.highScore;
            for (var i = 0; i < Global.activatedLevels[Global.indexOfSelectedLevel].stars; i++) {
                this.starsPause.children[i].active = true;
            }
            Global.gameState = Global.GAME_STATE.PAUSE;
            this.gamePause.active = true;
            this.gamePlay.active = false;
            this.n_control.active = false;
            this.gamePause.position = cc.v2();
        }
    },

    onResumeTouched() {
        if (Global.soundOn) cc.audioEngine.play(Global.click, false, 1);
        Global.gameState = Global.GAME_STATE.PLAYING;
        this.gamePause.active = false;
        this.gamePlay.active = true;
        this.n_control.active = true
        if (Global.selectedLevel == 11 && Global.selectedMode == 3) {
            this.gamePlay.getComponent("GamePlayController").setConnectedBody();
        }
        // this.gamePause.position = cc.v2();
    },

    onReplayTouched() {

        if (Global.soundOn) cc.audioEngine.play(Global.click, false, 1);
        Global.gameState = Global.GAME_STATE.PLAYING;
        this.gamePause.active = false;
        this.gameComplete.active = false;
        this.gamePlay.active = true;
        this.n_control.active = true;

        this.initStars();
        this.gamePlay.getComponent("GamePlayController").onReplayTouched();
    },

    onGameCompleted() {
        if (Global.soundOn) cc.audioEngine.play(Global.complete, false, 1);
        cc.log(Global.indexOfSelectedLevel);
        if (Global.indexOfSelectedLevel + 1 < 48) {
            Global.activatedLevels[Global.indexOfSelectedLevel + 1].unlocked = true;
        };
        cc.log(Global.indexOfSelectedLevel);
        var level = Global.selectedLevel + 1;
        this.lbNextLevel = "NEXT: LEVEL " + level + " - " +
            this.gamePlay.getComponent("GamePlayController").getModeTitle();
        Global.gameState = Global.GAME_STATE.COMPLETED;
        this.gameComplete.active = true;
        this.lbFinalScore.string = Global.score;
        this.gameComplete.position = cc.v2();
        this.gamePlay.active = false;
        this.n_control.active = false;
        var star = Global.score;
        if (star <= Global.maxScore / 3 || this.gamePlay.bulletsCount == 0) {
            star = 1;
        } else if (star <= 2 * Global.maxScore / 3) {
            star = 2;
        } else star = 3;
        this.runCompleteStarsAction(star);
        var _star = Global.activatedLevels[Global.indexOfSelectedLevel].stars
        Global.activatedLevels[Global.indexOfSelectedLevel].stars = star > _star ? star : _star;
        var highScore = Global.activatedLevels[Global.indexOfSelectedLevel].highScore;
        Global.activatedLevels[Global.indexOfSelectedLevel].highScore = Global.score > highScore ?
            Global.score : highScore;
        cc.sys.localStorage.setItem(Global.ACTIVATED_LEVEL, JSON.stringify(Global.activatedLevels));
    },

    onNextLevelTouched() {
        if (Global.soundOn) cc.audioEngine.play(Global.click, false, 1);
        this.gamePlay.getComponent("GamePlayController").onNextLevelTouched();
        this.initStars();
        if (Global.selectedLevel < 11) {
            Global.selectedLevel++;
            this.loadedMap = false;
            Global.selectedMap = null;
            Global.indexOfSelectedLevel++;
            Global.resourceLoader.loadMaps();
        } else if (Global.selectedMode < 3 && Global.selectedLevel == 11) {
            Global.selectedLevel = 0;
            Global.selectedMode++;
            this.setBackground(Global.selectedMode);
            this.loadedMap = false;
            Global.selectedMap = null;
            Global.indexOfSelectedLevel++;
            Global.resourceLoader.loadMaps();
            for (var i = 0; i <= Global.selectedMode; i++) {
                Global.activatedMode[i] = 1;
            }
            cc.sys.localStorage.setItem(Global.ACTIVATED_MODE, JSON.stringify(Global.activatedMode));
        } else if (Global.selectedLevel == 11) {
            if (Global.selectedMode == 3) {
                cc.director.loadScene('Menu Scene');
            }
        }

    },
    onBtnHelpCloseTouched() {
        if (Global.soundOn) cc.audioEngine.play(Global.click, false, 1);
        this.helpScreen.active = false;
    },
    update(dt) {
        if (this.loadedMap == false && Global.selectedMap != null) {
            this.loadedMap = true;
            this.gameComplete.active = false;
            this.gamePlay.active = true;
            this.n_control.active = true;
            this.gamePlay.getComponent("GamePlayController").onReplayTouched();

        }
        this.updateScore(dt);
    },

    updateScore(dt) {
        var score = Number(this.lbScore.string);
        if (score < Global.score) {
            score += 100;
            this.lbScore.string = score;
        }
    },
});
