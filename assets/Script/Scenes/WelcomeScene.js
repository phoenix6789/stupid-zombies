var Global = require('Global')
cc.Class({
    extends: cc.Component,

    properties: {
        splash: cc.Node,
        background: cc.Node,
        playAndSetting: cc.Node,
        settingItems: cc.Node,
        tgSound: cc.Node,
        btnHelp: cc.Node,
        btnClose: cc.Node,
        btnBooster: cc.Node,
        btnRate: cc.Node,
        btnFbShare: cc.Node,
        helpScreen: cc.Node,
        busted: cc.AudioClip,
        click: cc.AudioClip,
        complete: cc.AudioClip,
        explode: cc.AudioClip,
        killed: cc.AudioClip,
        music: cc.AudioClip,
        star: cc.AudioClip,
        gun: cc.AudioClip,
        gunLoad: cc.AudioClip,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.splash.runAction(cc.sequence(cc.delayTime(1), cc.fadeOut(0.8)));
        this.setAudio();
        cc.audioEngine.play(Global.music,true,1);
    },

    start() {

    },

    setAudio() {
        Global.music = this.music;
        Global.explode = this.explode;
        Global.busted = this.busted;
        Global.click = this.click;
        Global.complete = this.complete;
        Global.killed = this.killed;
        Global.gun = this.gun;
        Global.star = this.star;
        Global.gunLoad = this.gunLoad;
    },

    onBtnPlayTouched() {
        if (Global.soundOn) cc.audioEngine.play (Global.click, false, 1);
        this.node.runAction(cc.sequence(cc.delayTime(1), Global.disappear, cc.callFunc(function () {
            cc.director.loadScene("Menu Scene");
            Global.menuSceneLoadMode = true;
        })));
    },


    onBtnSettingTouched() {
        if (Global.soundOn) cc.audioEngine.play (Global.click, false, 1);
        this.playAndSetting.active = false;
        this.settingItems.active = true;
        this.btnBooster.scale = this.btnClose.scale = this.btnFbShare.scale
            = this.btnHelp.scale = this.btnRate.scale = this.tgSound.scale = 0;
        this.btnBooster.runAction(cc.scaleTo(0.5, 1).easing(cc.easeBackInOut()));
        this.btnClose.runAction(cc.scaleTo(0.5, 1).easing(cc.easeBackInOut()));
        this.btnFbShare.runAction(cc.scaleTo(0.5, 1).easing(cc.easeBackInOut()));
        this.btnHelp.runAction(cc.scaleTo(0.5, 1).easing(cc.easeBackInOut()));
        this.btnRate.runAction(cc.scaleTo(0.5, 1).easing(cc.easeBackInOut()));
        this.tgSound.runAction(cc.scaleTo(0.5, 1).easing(cc.easeBackInOut()));
    },

    onBtnCloseTouched() {
        if (Global.soundOn) cc.audioEngine.play (Global.click, false, 1);
        this.settingItems.active = false;
        this.playAndSetting.active = true;
    },

    onBtnHelpTouched() {
        if (Global.soundOn) cc.audioEngine.play (Global.click, false, 1);
        this.helpScreen.active = true;
    },

    onBtnHelpCloseTouched() {
        if (Global.soundOn) cc.audioEngine.play (Global.click, false, 1);
        this.helpScreen.active = false;
        this.playAndSetting.active = true;
        this.settingItems.active = false;
    },

    onTgSoundCheckChanged(toggle) {
        
        if (toggle.isChecked){
            cc.audioEngine.stopAll();
        }else{
            cc.audioEngine.play (Global.music, true, 1);
        }
        Global.soundOn = !toggle.isChecked;

    },

    onBtnFaceBookTouched(){

    },

    onBtnGiftBoxTouched(){

    },

    onBtnLikeTouched(){

    },

    // update (dt) {},
});
