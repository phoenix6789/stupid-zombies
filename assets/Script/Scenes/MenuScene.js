var Global = require('Global');

const Mode = cc.Enum({
    CITY: 0,
    JUNGLE: 1,
    LAB: 2,
    DESERT: 3
});

cc.Class({
    extends: cc.Component,

    properties: {
        parentMenu: cc.Node,
        childMenu: cc.Node,
        childMenuBackground: cc.Sprite,
        modes: {
            default: [],
            type: cc.Node,
        },

        levelItems: {
            default: [],
            type: cc.Node
        }
    },
    onLoad() {

        this.unlockedLevels = []; // array of level in selected mode
        Global.Mode = Mode;
        Global.menuScene = this;
        this.sound = this.parentMenu.getChildByName("tg sound");
        this.sound.getComponent(cc.Toggle).isChecked = !Global.soundOn;
        var activatedMode = JSON.parse(cc.sys.localStorage.getItem(Global.ACTIVATED_MODE));
        if (activatedMode == null) {
            activatedMode = [1, 0, 0, 0];
            cc.sys.localStorage.setItem(Global.ACTIVATED_MODE, JSON.stringify(activatedMode));
        }
        Global.activatedMode = activatedMode;
        if (Global.isTest) {
            Global.activatedMode = [1, 1, 1, 1];
        }
        for (var i = 1; i < 4; i++) {
            cc.log (Global.activatedMode[i]);
            if (Global.activatedMode[i] == 1) {
                cc.log (this.modes[i].children[0]);
                this.modes[i].children[0].active = false;
            } else this.modes[i].children[0].active = true;
        }

        var activatedLevels = JSON.parse(cc.sys.localStorage.getItem(Global.ACTIVATED_LEVEL));

        if (activatedLevels == null) {
            activatedLevels = [];
            activatedLevels = this.setActivatedLevels();
            cc.sys.localStorage.setItem(Global.ACTIVATED_LEVEL, JSON.stringify(activatedLevels));
        }
        if (Global.soundOn) cc.audioEngine.stopAll();
        if (Global.soundOn) cc.audioEngine.play(Global.music, true, 1);
        Global.activatedLevels = activatedLevels;
    },

    start() {
        var self = this;

        this.parentMenu.position = this.childMenu.position = cc.v2(0, 0);
        this.parentMenu.opacity = this.childMenu.opacity = 0;

        if (Global.menuSceneLoadMode == true) {
            this.childMenu.active = false;
            this.parentMenu.runAction(Global.appear);

        } else {
            this.parentMenu.active = false;
            this.childMenu.runAction(Global.appear);
            this.loadMode();
        }
    },

    setActivatedLevels() {
        var activatedLevels = []
        for (var i = 0; i < 48; i++) {
            var levelData = {
                index: 0,
                mode: 0,
                unlocked: false,
                stars: 0,
                highScore: 0,
            }
            levelData.unlocked = i == 0 ? true : false;
            levelData.mode = Math.floor(i / 12);
            levelData.index = i;
            cc.log(levelData.mode);
            activatedLevels.push(levelData);
        }
        return activatedLevels;
    },

    onCityModeTouched() {
        if (Global.soundOn) cc.audioEngine.play(Global.click, false, 1);
        Global.selectedMode = Mode.CITY;
        var self = this;
        Global.resourceLoader.loadSkins(1, 2);
        this.parentMenu.runAction(cc.sequence(cc.delayTime(0.5),
            cc.callFunc(function () {
                self.parentMenu.active = false;
                self.childMenu.active = true;
                self.loadMode();
            })));

    },

    onJungleModeTouched() {
        if (Global.soundOn) cc.audioEngine.play(Global.click, false, 1);
        if (Global.activatedMode[1] != 0) {

            Global.selectedMode = Mode.JUNGLE;
            Global.resourceLoader.loadSkins(4, 5);
            var self = this;
            this.parentMenu.runAction(cc.sequence(cc.delayTime(0.5),
                cc.callFunc(function () {
                    self.parentMenu.active = false;
                    self.childMenu.active = true;
                    self.loadMode();
                })));
        }
    },

    onLabModeTouched() {
        if (Global.soundOn) cc.audioEngine.play(Global.click, false, 1);
        if (Global.activatedMode[2] != 0) {

            Global.selectedMode = Mode.LAB;
            var self = this;
            Global.resourceLoader.loadSkins(2, 3);
            this.parentMenu.runAction(cc.sequence(cc.delayTime(0.5),
                cc.callFunc(function () {
                    self.parentMenu.active = false;
                    self.childMenu.active = true;
                    self.loadMode();
                })));
        }

    },

    onDesertModeTouched() {
        if (Global.soundOn) cc.audioEngine.play(Global.click, false, 1);
        if (Global.activatedMode[3] != 0) {
            Global.selectedMode = Mode.DESERT;
            var self = this;
            Global.resourceLoader.loadSkins(6, 6);
            this.parentMenu.runAction(cc.sequence(cc.delayTime(0.5),
                cc.callFunc(function () {
                    self.parentMenu.active = false;
                    self.childMenu.active = true;
                    self.loadMode();
                })));
        }
    },

    onChildMenuBackTouched() {
        if (Global.soundOn) cc.audioEngine.play(Global.click, false, 1);
        var self = this;
        this.childMenu.runAction(cc.sequence(cc.delayTime(0.5), cc.callFunc(
            function () {
                self.parentMenu.active = true;
                self.parentMenu.runAction(Global.appear);
                self.childMenu.opacity = 0;
                self.childMenu.active = false;
            }
        )));

    },

    onParentMenuBackTouched() {
        if (Global.soundOn) cc.audioEngine.play(Global.click, false, 1);
        var self = this;
        this.parentMenu.runAction(cc.sequence(cc.delayTime(0.5), cc.callFunc(
            function () {
                cc.director.loadScene("Welcome Scene");
            }
        )));

    },

    loadMode() {
        this.parentMenu.active = false;
        this.childMenu.active = true;
        var self = this;
        this.childMenu.runAction(cc.sequence(cc.callFunc(function () {
            self.setMenuBackground(Global.selectedMode);
        }), cc.delayTime(0.2), cc.callFunc(function () {
            self.setLevelProps(Global.selectedMode);
        }), Global.appear));
    },

    setMenuBackground(selectedMode) {
        switch (selectedMode) {
            case Mode.CITY: {
                this.childMenuBackground.spriteFrame = Global.sf_bg_city;
                break;
            }
            case Mode.JUNGLE: {
                this.childMenuBackground.spriteFrame = Global.sf_bg_jungle;
                break;
            }
            case Mode.LAB: {
                this.childMenuBackground.spriteFrame = Global.sf_bg_lab;
                break
            }
            case Mode.DESERT: {
                this.childMenuBackground.spriteFrame = Global.sf_bg_desert;
                break;
            }
        }
    },

    setLevelProps(selectedMode) {
        this.unlockedLevels = Global.activatedLevels.filter(data => {
            return data.mode == Global.selectedMode;
        });
        for (var i = 0; i < 12; i++) {
            this.levelItems[i].getComponent("ItemLevelManager").setBackground(selectedMode);
            this.levelItems[i].isUnlocked = this.unlockedLevels[i].unlocked;
            this.levelItems[i].index = this.unlockedLevels[i].index;
            if (this.unlockedLevels[i].unlocked) {
                this.levelItems[i].getComponent("ItemLevelManager").unlockLevel();
                this.levelItems[i].getComponent("ItemLevelManager").setStars(this.unlockedLevels[i].stars);
            } else {
                this.levelItems[i].getComponent("ItemLevelManager").lockLevel();
            }
        }

    },

    onTgSoundCheckChanged(toggle) {
        // if (Global.soundOn) cc.audioEngine.play (Global.click, false, 1);
        if (toggle.isChecked) {
            cc.audioEngine.stopAll();
        } else {
            cc.audioEngine.play(Global.music, true, 1);
        }
        Global.soundOn = !toggle.isChecked;

    }

    // update(dt) {
    //     cc.log(this);
    // },
});
