var global = require('Global');

cc.Class({
    extends: cc.Component,

    properties: {
        touchHandler: cc.Node,
        spine: cc.Node,
        sprite: cc.Node,
        upperBody: cc.Node,
        cross: cc.Node,
        n_fire: cc.Node,
        p1: cc.Node,
        p2: cc.Node,
        
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        var self = this;
        this.originalPointSetted = false;
        this.parent = this.node.parent;
        this.upperBodyGraphics = this.upperBody.getComponent(cc.Graphics);
        this.cross.opacity = 0;
        this.rotDegree = 0;
        this.vec = cc.v2();
        this.fired = false;
        this.sprite.opacity = 255;
        this.spine.active = false;
        // this.spSkeleton = this.spine.getComponent(sp.Skeleton);
        // this.maincontroller = this.spSkeleton.findBone("maincontroller");
        // this.spSkeleton.setCompleteListener((trackEntry, loopCount) => {
        //     var animationName = trackEntry.animation ? trackEntry.animation.name : "";
        //     if (animationName === 'fire') {
        //         this.spSkeleton.clearTrack(1);
        //         this.spSkeleton.setAnimation(0,'breathing',true);
        //     }
        // });
    },

    start() {


    },

    aim(target) {
        var _target = this.upperBody.convertToNodeSpace(target);
        var origin = this.sprite.convertToWorldSpace(this.upperBody.position);
        this.rotDegree = Math.atan2(target.y - origin.y, target.x -
            origin.x) * 180 / Math.PI;

        if (this.rotDegree > 90 || this.rotDegree < -90) {
            this.node.runAction(cc.flipX(true));
            
            this.upperBody.rotation = 180 + this.rotDegree;
            // this.maincontroller.rotation =  - 180 - this.rotDegree;
        } else {
            this.node.runAction(cc.flipX(false));
            this.upperBody.rotation = - this.rotDegree;
            // this.maincontroller.rotation = this.rotDegree;
        }

        //-------------
        this.upperBodyGraphics.clear();
        this.upperBodyGraphics.moveTo(this.p1.x, this.p1.y);
        var lineP = this.linearEquation(1500);
        this.upperBodyGraphics.lineTo(lineP.x, lineP.y);
        this.upperBodyGraphics.stroke();
        this.upperBodyGraphics.fill();
        //-------------
        this.cross.opacity = 255;
        this.cross.position = this.linearEquation(_target.x);
    },

    resetRotation(){
        this.upperBody.rotation = 0;
        this.node.runAction(cc.flipX(false));
    },

    fire(){
        this.n_fire.getComponent(cc.Animation).play();
        var action = cc.sequence(cc.rotateBy(0.1,-30), cc.rotateBy(0.1,30));
        this.upperBody.runAction(action);
    },

    getBulletRotation() {
        var _p1 = this.upperBody.convertToWorldSpace(this.p1.position);
        var _p2 = this.upperBody.convertToWorldSpace(this.p2.position);
        this.vec = _p2.sub(_p1);
        return Math.atan2(this.vec.y, this.vec.x) ;
    },

    linearEquation(x) {
        return cc.v2(x, ((this.p2.y - this.p1.y) / (this.p2.x - this.p1.x)) *
            (x - this.p1.x) + this.p1.y);
    },
    linearEquation1(x, _p1, _p2) {
        return cc.v2(x, ((_p2.y - _p1.y) / (_p2.x - _p1.x)) *
            (x - _p1.x) + _p1.y);
    },

    getP2WorldSpace() {
        return this.upperBody.convertToWorldSpace(this.p2.position);
    },

    update(dt) {
    },
});
