var Global = require('Global');
cc.Class({
    extends: cc.Component,

    properties: {
        levelUnlocked: cc.Node,
        levelLocked: cc.Node,

        stars: {
            default: [],
            type: cc.Sprite
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

    },

    start() {
        var self = this;
        this.loadResources();

        this.sp_levelUnlocked = this.levelUnlocked.getComponent(cc.Sprite);
        this.sp_levelLocked = this.levelLocked.getComponent(cc.Sprite);
        this.levelLocked.active = true;
        this.levelUnlocked.active = true;
        this.node.opacity = 0;
        this.node.on(cc.Node.EventType.TOUCH_START, function () {
            if (Global.soundOn) cc.audioEngine.play(Global.click, false, 1);
            if (Global.isTest) {
                Global.indexOfSelectedLevel = self.node.index;
                Global.selectedLevel = Global.menuScene.levelItems.indexOf(self.node);
                Global.menuScene.childMenu.runAction(cc.sequence(cc.callFunc(function () {
                    Global.resourceLoader.loadMaps();
                }), cc.delayTime(0.2), cc.callFunc(function () {
                    cc.director.loadScene('Game Scene');
                })));
            } else if (self.node.isUnlocked) {
                Global.indexOfSelectedLevel = self.node.index;
                Global.selectedLevel = Global.menuScene.levelItems.indexOf(self.node);
                Global.menuScene.childMenu.runAction(cc.sequence(cc.callFunc(function () {
                    Global.resourceLoader.loadMaps();
                }), cc.delayTime(0.2), cc.callFunc(function () {
                    cc.director.loadScene('Game Scene');
                })));
            }
        })
    },

    loadResources() {
        if (Global.resourceLoader != null) {
            this.sf_star = Global.resourceLoader.loadSpriteFrame("level/gold.png");
            this.sf_blackStar = Global.resourceLoader.loadSpriteFrame("level/black.png");

            this.sf_bgLocked_City = Global.resourceLoader.loadSpriteFrame("level/lockbox_0.png");
            this.sf_bgLocked_Jungle = Global.resourceLoader.loadSpriteFrame("level/lockbox_1.png");
            this.sf_bgLocked_Lab = Global.resourceLoader.loadSpriteFrame("level/lockbox_2.png");
            this.sf_bgLocked_Desert = Global.resourceLoader.loadSpriteFrame("level/lockbox_3.png");

            this.sf_bgUnlocked_City = Global.resourceLoader.loadSpriteFrame("level/unlockbox_0.png");
            this.sf_bgUnlocked_Jungle = Global.resourceLoader.loadSpriteFrame("level/unlockbox_1.png");
            this.sf_bgUnlocked_Lab = Global.resourceLoader.loadSpriteFrame("level/unlockbox_2.png");
            this.sf_bgUnlocked_Desert = Global.resourceLoader.loadSpriteFrame("level/unlockbox_3.png");
        }
    },

    lockLevel() {
        this.levelLocked.active = true;
        this.levelUnlocked.active = false;
    },

    unlockLevel() {
        this.levelLocked.active = false;
        this.levelUnlocked.active = true;
    },

    setBackground(mode) {
        switch (mode) {
            case Global.Mode.CITY: {
                this.sp_levelLocked.spriteFrame = this.sf_bgLocked_City;
                this.sp_levelUnlocked.spriteFrame = this.sf_bgUnlocked_City;
                break;
            }
            case Global.Mode.JUNGLE: {
                this.sp_levelLocked.spriteFrame = this.sf_bgLocked_Jungle;
                this.sp_levelUnlocked.spriteFrame = this.sf_bgUnlocked_Jungle;
                break;
            }
            case Global.Mode.LAB: {
                this.sp_levelLocked.spriteFrame = this.sf_bgLocked_Lab;
                this.sp_levelUnlocked.spriteFrame = this.sf_bgUnlocked_Lab;
                break;
            }
            case Global.Mode.DESERT: {
                this.sp_levelLocked.spriteFrame = this.sf_bgLocked_Desert;
                this.sp_levelUnlocked.spriteFrame = this.sf_bgUnlocked_Desert;
                break;
            }
        }
        this.node.opacity = 255;
    },

    setStars(gotStars) {
        switch (gotStars) {
            case 0: {
                this.stars[0].spriteFrame = this.sf_blackStar;
                this.stars[1].spriteFrame = this.sf_blackStar;
                this.stars[2].spriteFrame = this.sf_blackStar;
                break;
            }
            case 1: {
                this.stars[0].spriteFrame = this.sf_star;
                this.stars[1].spriteFrame = this.sf_blackStar;
                this.stars[2].spriteFrame = this.sf_blackStar;
                break;
            }
            case 2: {
                this.stars[0].spriteFrame = this.sf_star;
                this.stars[1].spriteFrame = this.sf_star;
                this.stars[2].spriteFrame = this.sf_blackStar;
                break;
            }
            case 3: {
                this.stars[0].spriteFrame = this.sf_star;
                this.stars[1].spriteFrame = this.sf_star;
                this.stars[2].spriteFrame = this.sf_star;
                break;
            }
        }
    },



    // update (dt) {},
});
