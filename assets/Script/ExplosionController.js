var Global = require("Global");

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.collisionList = [];
        this.isExploding = false;
        this.parentBlast = false;
    },

    start() {
        this.collisionList = [];
    },

    // onCollisionEnter(other, self) {
    //     // if (this.collisionList.indexOf(otherCollider.node.uuid) == -1) {
    //     this.collisionList.push(other.node.uuid);
    //     this.exploding(other.node);
    //     // cc.log("onload explosion controller");
    //     cc.log("onload explosion controller", other.node);
    //     // }
    // },

    onCollisionStay(other, self) {

    },

    onBeginContact(contact, selfCollider, otherCollider) {
          
        if (otherCollider.node.name == "explosive" && this.isExploding == false
            && otherCollider.node.parent.blasted == true){
            this.isExploding = true;
            // cc.log ()
        }
        if (this.collisionList.indexOf(otherCollider.node.uuid) == -1) {
            this.collisionList.push(otherCollider.node.uuid);
            this.exploding(otherCollider.node);
        }
        
    },
    // onPresolve(contact, selfCollider, otherCollider) {

    // },

    exploding(node) {
        var parent = this.node.parent;
        var pointExplosion = cc.v2(parent.x, parent.y - parent.height / 2);
        var distance = node.position.sub(pointExplosion);
        var speed = Global.EXPLOSIVE_SPEED * Global.EXPLOSIVE_RADIUS *
            this.node.getComponent(cc.PhysicsCircleCollider).radius / distance.mag();
        node.getComponent(cc.RigidBody).linearVelocity = distance.normalize().mulSelf(speed);

    },

    update(dt) {
        if (this.node.position != cc.v2()) {
            this.node.position = cc.v2();
        }
        if (this.isExploding) {
            if (this.parentBlast == false){
                this.node.parent.getComponent("BombController").blastAnimate();
                this.parentBlast = true;
            }
            this.node.parent.getComponent(cc.Sprite).enabled = false; 
            if (this.node.scale <= Global.EXPLOSIVE_RADIUS) {
                this.node.scale += 2;
            } else {
                this.node.parent.getComponent(cc.RigidBody).active = false; 
                this.node.active = false;
            }
        }

    },
});
