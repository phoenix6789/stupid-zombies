var Global = require("Global");
cc.Class({
    extends: cc.Component,

    properties: {
        endjoint: cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        if (this.node.name == "piperun") {
            // this.node.getComponent(cc.ParticleSmoke).stopSystem();
        } else {
            this.node.getComponent(cc.CircleCollider).enabled = false;
        }
    },

    start() {
    },

    onCollisionEnter(other, self) {
        if (this.node.name == "piperun") {
            if (other.node.parent.name == "bullet") {
                this.node.getComponent(cc.Animation).play();
                if (this.node.endjoint != null) {
                    this.node.endjoint.getChildByName("particle").getComponent(cc.ParticleSystem).resetSystem();
                    this.node.endjoint.getComponent(cc.CircleCollider).enabled = true;
                }
            }
        } else {
            if (Global.killerObjects.indexOf(other.node.name) != -1) {
                var self = this;
                other.node.getComponent(cc.RigidBody).linearVelocity = 
                    other.node.position.sub(this.node.position).normalize().mulSelf(300);
                    this.node.runAction(cc.sequence(cc.delayTime(2), cc.callFunc(
                        function () {
                            self.node.getComponent(cc.CircleCollider).enabled = false;
                        }
                    )))
            }
        }
    },

    // update (dt) {},
});
